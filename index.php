<?php 

	require_once ('matrix.php');	
	$num = array(1,2,3,4,5,6,7,8,9);	
	
	$y=0;	
	
?>
<style>
	table {
		border-color: #eee;
		margin-bottom: 20px;
	}
	.sum-tr {
		text-align: right;
		color: green;
	}
</style>

<?php for($y;$y<30;$y++): ?>
	<?php shuffle($num); ?>
	<p>матрица № <?php echo $y + 1; ?></p>
	<table cellspacing="0" border="1">
		<?php matrix::matrixCol($num); ?>		
	</table>
<?php endfor; ?>